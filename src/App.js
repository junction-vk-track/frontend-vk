import React, { useState, useEffect } from 'react';
import connect from '@vkontakte/vk-connect';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';

import Home from './panels/Home';
import MyWishlist from './panels/MyWishlist';
import Friends from './panels/Friends';
import Category from './panels/Category';
import FWishlist from './panels/FWishlist';
import Main from './panels/Main';


const App = () => {
	const [activePanel, setActivePanel] = useState('main');
	const [fetchedUser, setUser] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);

	useEffect(() => {
		connect.subscribe(({ detail: { type, data }}) => {
			// console.log('subccribe', type, data);
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}

		});
		async function fetchData() {
			const user = await connect.sendPromise('VKWebAppGetUserInfo');
			setUser(user);
			setPopout(null);
		}
		fetchData();
	}, []);

	const go = e => {
		console.log('go', e, e.currentTarget, e.currentTarget.dataset);
		setActivePanel(e.currentTarget.dataset.to);
	};

	return (
		<View style={{ overflowY: 'hidden'}} activePanel={activePanel} popout={popout}>
			<Main id="main" fetchedUser={fetchedUser} go={go}/>
			<Home id='home' fetchedUser={fetchedUser} go={go} />
			<MyWishlist id='persik' go={go} user={fetchedUser} />
			<Friends id='friends' go={go} />
			<Category id='category' go={go} user={fetchedUser}/>
			<FWishlist id="f_wishlist" go={go}/>
		</View>
	);
}

export default App;

