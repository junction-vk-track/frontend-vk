import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
// import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
// import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
// import connect from '@vkontakte/vk-connect';
import { platform, IOS } from '@vkontakte/vkui';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';

import Wishlist from '../utils/Wishlist';

import './category.css';

const axios = require('axios');

const osName = platform();

const MyWishlist = props => {

	
	const wishlist = Wishlist.wishlist;

	const [items, setItems] = useState(wishlist.list);

	const getItemsByIds = (wishlist) => {
		/* if (wishlist.list.length > 0) {
			connect.sendPromise("VKWebAppCallAPIMethod", { "method": "junction.getByIds", "request_id": "32test", "params": { "ids": wishlist.list.join(','), "offset": 0, "v":"5.103", "access_token": localStorage.getItem('access_token')}}).then(data => {
				console.log('daata promise', data.response);
				setItems(data.response);
			});
		} */
		setItems(wishlist.list);
	}


	useEffect(() => {

		/* if (wishlist.list.length > 0)
        	getItemsByIds(wishlist); */
		console.log('useEffect');
		Wishlist.onChange(getItemsByIds);

		return () => {
			Wishlist.offChange(getItemsByIds);
		};
		
	}, []);


	function removeItemFromWishlist (id) {
		axios.post('/api/remove_from_wishlist', {
			user_id: props.user.id,
			product_id: id
		}).then(data => {
			console.log('removeItemFromWishlist', data);
			Wishlist.removeFromWishlist(id);
		}).catch(e => {
			Wishlist.removeFromWishlist(id);
		})
		
	}


	return (
		<Panel id={props.id}>
			<PanelHeader
				left={<HeaderButton onClick={(e) => { console.log('back'); props.go(e);}} data-to="home">
					{osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
				</HeaderButton>}
			>
				My Wishlist
			</PanelHeader>
			
			<Group>
				<Div style={{ overflowY: 'scroll', height: '440px'  }} className="category">
					{ items.length === 0 ? 'You have empty wishlist' : ''}
					{
						items && items.map((item, i) => {
							return (
								<div className="item" key={i}>
								<a target="_blank" rel="noopener noreferrer" href={item.url}>
									<img className="item_img" width={180} height={180} src={item.picture} alt={item.name}/>
								</a>
								<p>{item.description}</p>
								<h3>{item.name}</h3>
								<h5>{`Price: ${Math.floor(item.price /100)} $`}</h5>
								<button onClick={removeItemFromWishlist.bind(this, item.id)}> Remove from Wishlist</button>
								</div>
							);
						})
					}
				</Div>
			</Group>
		</Panel>
	);
}

MyWishlist.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default MyWishlist;
