import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import connect from '@vkontakte/vk-connect';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import { platform, IOS } from '@vkontakte/vkui';


import './Home.css';

const osName = platform();

const Home = ({ id, go, fetchedUser }) => {

	const [cats, setCategories] = useState(null);

	useEffect(() => {
		
		connect.sendPromise("VKWebAppGetAuthToken", {"app_id": 7209834, "scope": "friends,status,market"}).then(data => {
			console.log('access_token', data.access_token);
			localStorage.setItem('access_token', data.access_token);
		}).then (() => {
			connect.sendPromise("VKWebAppCallAPIMethod", { "method": "junction.getCategories", "request_id": "32test", "params": { "count": "50", "v":"5.103", "access_token": localStorage.getItem('access_token')}}).then(data => {
				console.log('daata promise', data.response);
				setCategories(data.response);
			});
		})
		
	}, []);

	function goToCategory(e) {
		console.log(e);
		localStorage.setItem('category', JSON.stringify({ id: e.currentTarget.id, name: e.currentTarget.name }));
		go(e);
	}
	
	return (
	<Panel id={id}>
		<PanelHeader
			left={<HeaderButton onClick={go} data-to="main">
				{osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
			</HeaderButton>}
		>
			Goods
		</PanelHeader>
		{fetchedUser &&
		<Group>
		<Cell
			before={fetchedUser.photo_200 ? <Avatar src={fetchedUser.photo_200}/> : null}
			description={fetchedUser.city && fetchedUser.city.title ? fetchedUser.city.title : ''}
		>
			  {`${fetchedUser.first_name} ${fetchedUser.last_name}`}
                <div style={{ position: 'absolute', right: '12px', display: 'flex', top: '15px', justifyContent: 'flex-end' }}>
                    <Button style={{ width: '110px' }} size="m" level="2" onClick={go} data-to="persik">
                        My Wishlist
                    </Button>
                    &nbsp;&nbsp;
                    <Button style={{ width: '110px' }} size="m" level="2" onClick={go} data-to="friends">
                        Friends
                    </Button>
                </div>
		</Cell>
	</Group>}

		<Group>
			<Div>
				<div style={{ height: '345px' }} className="scrollcontainer">
				{
					cats && cats.map(cat => {
						return (
							<Button style={{ margin: '5px'}} key={cat.id} name={cat.name} id={cat.id} size="xl" level="2" onClick={goToCategory} data-to="category">
							 {cat.name}
							</Button>
						);
					})
				}
				</div>	
			</Div>
		</Group>
	</Panel>
);

}

Home.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
	fetchedUser: PropTypes.shape({
		photo_200: PropTypes.string,
		first_name: PropTypes.string,
		last_name: PropTypes.string,
		city: PropTypes.shape({
			title: PropTypes.string,
		}),
	}),
};

export default Home;
