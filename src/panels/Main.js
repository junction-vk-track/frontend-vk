import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// import connect from '@vkontakte/vk-connect';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import axios from 'axios';
import Wishlist from '../utils/Wishlist';

import './Home.css';

const Main = ({ id, go, fetchedUser }) => {

    const userId = fetchedUser && fetchedUser.id;

	const [items, setItems] = useState([
        {
            category_id: "3",
            currency_id: "1",
            description: "Katya T Shirt UNHhhh T-Shirt Printed Basic Graphic Tee Shirt Big Short Sleeves Man 100 Percent",
            id: "12",
            name: "Katya T Shirt UNHhhh T-Shirt Printed Basic Graphic Tee Shirt Big Short Sleeves Man 100 Percent",
            old_price: "1102",
            picture: "https://ae01.alicdn.com/kf/ULB83jTssTzIXKJkSafVq6yWgXXaG/Katya-T-Shirt-UNHhhh-T-Shirt-Printed-Basic-Graphic-Tee-Shirt-Big-Short-Sleeves-Man-100.jpg",
            price: "1900",
            product_id: "1000008129638",
            rating: 4.1,
            url: "https://www.aliexpress.com/item/Katya-T-Shirt-UNHhhh-T-Shirt-Printed-Basic-Graphic-Tee-Shirt-Big-Short-Sleeves-Man-100/1000008129638.html?src=facebook&aff_short_key=cCZtBY5p&isdl=y&aff_platform=true"
        },
        {
            category_id: "3",
            currency_id: "1",
            description: "Customized autumn and winter male jacket wear thick retro retro gothic style lolita metal rivet",
            id: "68",
            name: "Customized autumn and winter male jacket wear thick retro retro gothic style lolita metal rivet",
            old_price: "8554",
            picture: "https://ae01.alicdn.com/kf/HTB1q8jBJwmTBuNjy1Xbq6yMrVXat/Customized-autumn-and-winter-male-jacket-wear-thick-retro-retro-gothic-style-lolita-metal-rivet-stitching.jpg",
            price: "9100",
            product_id: "32907332302",
            rating: 4.9,
            url: "https://www.aliexpress.com/item/Customized-autumn-and-winter-male-jacket-wear-thick-retro-retro-gothic-style-lolita-metal-rivet-stitching/32907332302.html?src=facebook&aff_short_key=cCZtBY5p&isdl=y&aff_platform=true"
        }
    ]);

    const [wish, setWish] = useState(Wishlist.wishlist);

	const change = (wishlist) => {
		// setItems(wishlist.list);
		console.log('change!!!', wishlist);
		setWish(wishlist);
	}

	useEffect(() => {

		axios.get(`/api/get_recommendations/${userId}`).then(resp => {
            console.log(' reqomennd', resp.data);
            setItems(resp.data);
        });

        Wishlist.onChange(change);

		return () => {
			Wishlist.offChange(change);
		};
			
    }, [userId, Wishlist.wishlist]);
    
    console.log('user', fetchedUser);

    
    function addItemInWishlist (item) {
		console.log(item);
		axios.post('/api/remove_from_wishlist', {
			user_id: userId,
			product: item
		}).then(data => {
			console.log('addItemInWishlist', data);
			Wishlist.addToWishlist(item);
		}).catch(e => {
            Wishlist.addToWishlist(item);
        })
	}

	function removeItemFromWishlist (id) {
		axios.post('/api/remove_from_wishlist', {
			user_id: userId,
			product_id: id
		}).then(data => {
			console.log('removeItemFromWishlist', data);
			Wishlist.removeFromWishlist(id);
		}).catch(e => {
            Wishlist.removeFromWishlist(id);
        })
		
	}
	
	return (
	<Panel id={id}>
		<PanelHeader>True Gift</PanelHeader>
		{fetchedUser &&
		<Group>
			<Cell
				before={fetchedUser.photo_200 ? <Avatar src={fetchedUser.photo_200}/> : null}
				description={fetchedUser.city && fetchedUser.city.title ? fetchedUser.city.title : ''}
			>
                {`${fetchedUser.first_name} ${fetchedUser.last_name}`}
                <div style={{ position: 'absolute', right: '12px', display: 'flex', top: '15px', justifyContent: 'flex-end' }}>
                    <Button style={{ width: '110px' }} size="m" level="2" onClick={go} data-to="persik">
                        My Wishlist
                    </Button>
                    &nbsp;&nbsp;
                    <Button style={{ width: '110px' }} size="m" level="2" onClick={go} data-to="friends">
                        Friends
                    </Button>
                </div>
			</Cell>
		</Group>}

		<Group>
			<Div>
            <Button style={{ margin: '5px'}} size="xl" onClick={go} level="2" data-to="home">
			Categories
			</Button>
				<div style={{ display: "flex" }} className="scrollcontainer">
                {
                    items && items.map((item, i) => {
                        return (
							<div className="item" key={i}>
								<a target="_blank" rel="noopener noreferrer" href={item.url}>
									<img className="item_img" width={180} height={180} src={item.picture} alt={item.name}/>
								</a>
								<p>{item.description}</p>
								<h3>{item.name}</h3>
								<h5>{`Price: ${Math.floor(item.price /100)} $`}</h5>
								<div style={{ display: 'flex', justifyContent: 'space-between'}}>
								{ Wishlist.isItemInWishlist(item.id, wish) ? <button onClick={removeItemFromWishlist.bind(this, item.id)}> Remove from Wishlist</button> : <button onClick={addItemInWishlist.bind(this, item)}> Add to Wishlist</button> }
								<button>Buy</button>
								</div>
							</div>
                        );
                    })
                }
				</div>	
			</Div>
		</Group>
	</Panel>
);

}

Main.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
	fetchedUser: PropTypes.shape({
		photo_200: PropTypes.string,
		first_name: PropTypes.string,
		last_name: PropTypes.string,
		city: PropTypes.shape({
			title: PropTypes.string,
		}),
	}),
};

export default Main;
