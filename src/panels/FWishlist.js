import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
// import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
// import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import connect from '@vkontakte/vk-connect';
import { platform, IOS } from '@vkontakte/vkui';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';

import './category.css';

const axios = require('axios');

const osName = platform();

const FWishlist = props => {

	
    const friend = JSON.parse(localStorage.getItem('friend'));
    console.log('firiend', friend)
	const [items, setItems] = useState([
        {
            "id": "53",
            "product_id": "32987032184",
            "url": "https://www.aliexpress.com/item/32987032184.html?src=facebook&aff_short_key=cCZtBY5p&isdl=y&aff_platform=true",
            "picture": "https://ae01.alicdn.com/kf/HLB1r7jYLVzqK1RjSZSgq6ApAVXag/High-Quality-Self-Portrait-Dress-2019-Autumn-Sexy-Elegant-One-Shoulder-Blue-Party-Dress-Womens-Ruffles.jpg",
            "name": "High Quality Self Portrait Dress 2019 Autumn Sexy Elegant One Shoulder Blue Party Dress Womens",
            "price": "15291",
            "old_price": "10704",
            "currency_id": "1",
            "description": "High Quality Self Portrait Dress 2019 Autumn Sexy Elegant One Shoulder Blue Party Dress Womens",
            "category_id": "3",
            "rating": 4.9
        },
        {
            "id": "55",
            "product_id": "32862181666",
            "url": "https://www.aliexpress.com/item/32862181666.html?src=facebook&aff_short_key=cCZtBY5p&isdl=y&aff_platform=true",
            "picture": "https://ae01.alicdn.com/kf/HTB1km4Alf9TBuNjy1zbq6xpepXad/High-quality-Autumn-New-High-Fashion-Brand-Woman-Classic-Double-Breasted-Trench-Coat-Waterproof-Raincoat-Business.jpg",
            "name": "High quality Autumn New High Fashion Brand Woman Classic Double Breasted Trench Coat Waterproof",
            "price": "6555",
            "old_price": "3409",
            "currency_id": "1",
            "description": "High quality Autumn New High Fashion Brand Woman Classic Double Breasted Trench Coat Waterproof",
            "category_id": "3",
            "rating": 4.9
        }

    ]);

/* 	const getItemsByIds = (wishlist) => {
		/* if (wishlist.list.length > 0) {
			connect.sendPromise("VKWebAppCallAPIMethod", { "method": "junction.getByIds", "request_id": "32test", "params": { "ids": wishlist.list.join(','), "offset": 0, "v":"5.103", "access_token": localStorage.getItem('access_token')}}).then(data => {
				console.log('daata promise', data.response);
				setItems(data.response);
			});
		}
		setItems(wishlist.list);
	} */


	 useEffect(() => {
        axios.get(`https://gostkin.codes/api/get_wishlist/${friend.id}`).then(resp => {
            console.log('get 1', resp.data.wishlist);
            if (resp.data.wishlist.length > 0) {
                return connect.sendPromise("VKWebAppCallAPIMethod", { "method": "junction.getByIds", "request_id": "32test", "params": { "ids": resp.data.wishlist.join(','), "offset": 0, "v":"5.103", "access_token": localStorage.getItem('access_token')}})
            }
        }).then(data => {
            console.log('get 2', data);
            if (data && data.response)
                setItems(data.response);
        }, error => { console.log('error', error)})
	}, [friend.id]);

    const buy = () => {
        console.log(buy);
    }

    const sharedbuy = () => {
        console.log('sharedbuy');
    }


	return (
		<Panel id={props.id}>
			<PanelHeader
				left={<HeaderButton onClick={(e) => { console.log('back'); props.go(e);}} data-to="friends">
					{osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
				</HeaderButton>}
			>
				{`${friend.first_name}'s Wishlist`}
			</PanelHeader>
			
			<Group>
				<Div className="category">
					{ items.length === 0 ? 'You have empty wishlist' : ''}
					{
						items && items.map((item, i) => {
							return (
								<div className="item" key={i}>
                                    <a target="_blank" rel="noopener noreferrer" href={item.url}>
                                        <img className="item_img" width={180} height={180} src={item.picture} alt={item.name}/>
                                    </a>
                                    <p>{item.description}</p>
                                    <h3>{item.name}</h3>
                                    <h5>{`Price: ${Math.floor(item.price /100)} $`}</h5>
                                    <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                                        <button onClick={buy}>Buy for gift</button>
                                        <button onClick={sharedbuy}>Shared purchase</button>
                                    </div>
								</div>
							);
						})
					}
				</Div>
			</Group>
		</Panel>
	);
}

FWishlist.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default FWishlist;
