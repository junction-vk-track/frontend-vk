import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import connect from '@vkontakte/vk-connect';
import { platform, IOS } from '@vkontakte/vkui';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';


const Category = props => {

    const osName = platform();
    const cat = JSON.parse(localStorage.getItem('category'));

	const [items, setItems] = useState(null);

	useEffect(() => {
        connect.sendPromise("VKWebAppCallAPIMethod", { "method": "junction.getByCategory", "request_id": "32test", "params": { "category_id": cat.id, "offset": 0, "v":"5.103", "access_token": localStorage.getItem('access_token')}}).then(data => {
            console.log('daata promise', data.response);
            setItems(data.response);
        });
		
		
	}, [cat.id]);
	
	return (
	<Panel id={props.id}>
		<PanelHeader left={<HeaderButton onClick={(e) => { console.log('back'); props.go(e);}} data-to="home">
				{osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
			</HeaderButton>}
        >{cat.name}</PanelHeader>
		<Group>
			<Cell>
				<Button size="xl" level="2" onClick={props.go} data-to="persik">
					My Wishlist
				</Button>
				<br/>
				<Button size="xl" level="2" onClick={props.go} data-to="friends">
					Friends
				</Button>
			</Cell>
		</Group>

		<Group>
			<Div>
				{
                    items && items.map((item, i) => {
                        return (
                            <p key={i}>{item.name}</p>
                        );
                    })
                }
			</Div>
		</Group>
	</Panel>
);

}

Category.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
	fetchedUser: PropTypes.shape({
		photo_200: PropTypes.string,
		first_name: PropTypes.string,
		last_name: PropTypes.string,
		city: PropTypes.shape({
			title: PropTypes.string,
		}),
	}),
};

export default Category;
