import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import connect from '@vkontakte/vk-connect';
import { platform, IOS } from '@vkontakte/vkui';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Wishlist from '../utils/Wishlist';


import './category.css';

const axios = require('axios');



const Category = props => {


    const osName = platform();
	const cat = JSON.parse(localStorage.getItem('category'));

	//const wishlist = Wishlist.wishlist;
	let wishlist = useRef(Wishlist.wishlist);
	console.log(wishlist);
	
	const [items, setItems] = useState([]);
	const [wish, setWish] = useState(Wishlist.wishlist);

	const change = (wishlist) => {
		// setItems(wishlist.list);
		console.log('change!!!', wishlist);
		setWish(wishlist);
	}

	useEffect(() => {
        connect.sendPromise("VKWebAppCallAPIMethod", { "method": "junction.getByCategory", "request_id": "32test", "params": { "category_id": cat.id, "offset": 0, "v":"5.103", "access_token": localStorage.getItem('access_token')}}).then(data => {
            console.log('daata promise', data.response);
            setItems(data.response);
		});
		Wishlist.onChange(change);

		return () => {
			Wishlist.offChange(change);
		};
		
		
	}, [cat.id]);

	
	function addItemInWishlist (item) {
		console.log(item);
		axios.post('/api/remove_from_wishlist', {
			user_id: props.user.id,
			product: item
		}).then(data => {
			console.log('removeItemFromWishlist', data);
			Wishlist.addToWishlist(item);
		}).catch(e => {
			Wishlist.addToWishlist(item);
		})
	}

	function removeItemFromWishlist (id) {
		axios.post('/api/remove_from_wishlist', {
			user_id: props.user.id,
			product_id: id
		}).then(data => {
			console.log('removeItemFromWishlist', data);
			Wishlist.removeFromWishlist(id);
		}).catch(e => {
			Wishlist.removeFromWishlist(id);
		})
		
	}
	
	return (
	<Panel id={props.id}>
		<PanelHeader left={<HeaderButton onClick={(e) => { console.log('back'); props.go(e);}} data-to="home">
				{osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
			</HeaderButton>}
        >{cat.name}</PanelHeader>
		<Group >
			<div style={{ overflowY: 'scroll', height: '440px'  }} className="category">
				{
                    items && items.map((item, i) => {
                        return (
							<div className="item" key={i}>
								<a target="_blank" rel="noopener noreferrer" href={item.url}>
									<img className="item_img" width={180} height={180} src={item.picture} alt={item.name}/>
								</a>
								<p>{item.description}</p>
								<h3>{item.name}</h3>
								<h5>{`Price: ${Math.floor(item.price /100)} $`}</h5>
								<div style={{ display: 'flex', justifyContent: 'space-between'}}>
								{ Wishlist.isItemInWishlist(item.id, wish) ? <button onClick={removeItemFromWishlist.bind(this, item.id)}> Remove from Wishlist</button> : <button onClick={addItemInWishlist.bind(this, item)}> Add to Wishlist</button> }
								<button>Buy</button>
								</div>
							</div>
                        );
                    })
                }
			</div>
		</Group>
	</Panel>
);

}

Category.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
	fetchedUser: PropTypes.shape({
		photo_200: PropTypes.string,
		first_name: PropTypes.string,
		last_name: PropTypes.string,
		city: PropTypes.shape({
			title: PropTypes.string,
		}),
	}),
};

export default Category;
