import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { platform, IOS } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import connect from '@vkontakte/vk-connect';
import Button from '@vkontakte/vkui/dist/components/Button/Button';


import './Friends.css';

const osName = platform();

const Friends = props => {

    const [friends, setFriends] = useState([{
            id: 104002937,
            first_name: "Оксана",
            last_name: "Костюк",
            is_closed: true,
            can_access_closed: true,
            photo_200: "https://sun6-14.userapi.com/7iE94VXV8VXX8zxyGYSvIZwtX0Nndajk9kxmRg/iVce9yKX1A4.jpg?ava=1"
        },
        {
            can_access_closed: true,
            first_name: "Евгений",
            id: 75563326,
            is_closed: false,
            last_name: "Госткин",
            photo_200: "https://sun6-14.userapi.com/c853520/v853520171/14c920/CyX2tbvDi4c.jpg?ava=1"
        }
    ]);

    useEffect(() => {
        connect.sendPromise("VKWebAppCallAPIMethod", { "method": "friends.getAppUsers", "request_id": "32test", "params": { "v":"5.103", "access_token": localStorage.getItem('access_token')}}).then(data => {
            console.log('daata promise 1', data.response);
            const user_ids = data.response.join(",");
            return connect.sendPromise("VKWebAppCallAPIMethod", { "method": "users.get", "request_id": "33test", "params": { user_ids, fields: "photo_200", "v":"5.103", "access_token": localStorage.getItem('access_token')}})
		}).then(data => {
            console.log('daata promise 2', data);
            setFriends(data.response)
        });
		
    }, []);
    
    const chooseFriend = (e) => {
        console.log('chooseFriend', e.currentTarget, e.currentTarget.getAttribute('data'), friends[Number(e.currentTarget.getAttribute('data'))]);
        localStorage.setItem('friend', JSON.stringify(friends[e.currentTarget.getAttribute('data')]));
        props.go(e);
    }
    
    return (
	<Panel id={props.id}>
		<PanelHeader
			left={<HeaderButton onClick={props.go} data-to="home">
				{osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
			</HeaderButton>}
		>
			Friends
		</PanelHeader>
		<Div>
            <div style={{ overflowY: 'auto', height: '440px', display: 'flex'  }}>
            {
                friends.map((f, i) => {
                    console.log('ff', f);
                    return (
                        <div className="friend_item" key={i}>
                            <img alt="" className="friend_img" src={f.photo_200}/>
                            <p style={{ textAlign: 'center'}}>{`${f.first_name} ${f.last_name}`}</p>
                            <Button size="xl" level="2" data={i} onClick={chooseFriend} data-to="f_wishlist">
                                {`${f.first_name}'s wishlist`}
                            </Button>
                        </div>);
                })
            }
            </div>
        </Div>
    </Panel>);
    
}

Friends.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default Friends;
