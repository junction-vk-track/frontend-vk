
class Wishlist {

    listeners = [];

    constructor() {
        if (!localStorage.getItem('wishlist')) {
            localStorage.setItem('wishlist', JSON.stringify({ list: [] }));
        }
    }

    get wishlist () {
        return JSON.parse(localStorage.getItem('wishlist'));
    }

    save(wishlist) {
        console.log('save', wishlist);
        localStorage.setItem('wishlist', JSON.stringify(wishlist));
    }

    onChange(cb) {
        if (this.listeners.indexOf(cb) === -1) {
            this.listeners.push(cb)
        }
    }

    offChange(cb) {
        const index = this.listeners.findIndex(item => item === cb);
        if (index > -1) {
            this.listeners.splice(index, 1);
        }
    }

    broadcast() {
        for (let i = 0; i < this.listeners.length; i++) {
            this.listeners[i](this.wishlist);
        }
    }

    addToWishlist(item) {
        const wishlist = this.wishlist;
        if (wishlist.list.findIndex(it => it.id === item.id) === -1) {
            wishlist.list.push(item);
        }
        this.save(wishlist);
        this.broadcast();
    }

    removeFromWishlist(id) {
        const wishlist = this.wishlist;
        const index = wishlist.list.findIndex(item => item.id === id);
        if (index > -1) {
            wishlist.list.splice(index, 1);
        }
        this.save(wishlist);
        this.broadcast();
    }

    isItemInWishlist(id, wish) {
        const wishlist = wish || this.wishlist;
        const index = wishlist.list.findIndex(item => item.id === id);
        return index > -1;
    }


}

export default new Wishlist();
